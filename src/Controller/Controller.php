<?php

namespace Sda\Atrakcjetrojmiasta\Controller;

use Sda\Atrakcjetrojmiasta\Comment\Comment;
use Sda\Atrakcjetrojmiasta\Comment\CommentRepository;
use Sda\Atrakcjetrojmiasta\Config\Routing;
use Sda\Atrakcjetrojmiasta\Request\Request;
use Sda\Atrakcjetrojmiasta\Template\Template;
use Sda\Atrakcjetrojmiasta\Offerts\OffertsRepository;
use Sda\Atrakcjetrojmiasta\User\User;
use Sda\Atrakcjetrojmiasta\User\UserRepository;



class Controller
{
    private $request;
    private $template;
    private $repo;
    private $params = [];
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var CommentRepository
     */
    private $commentsRepo;

    public function __construct(Request $request, Template $template, OffertsRepository $repo,
                                UserRepository $userRepository, CommentRepository $commentsRepo)
    {
        $this->request = $request;
        $this->template = $template;
        $this->repo = $repo;
        $this->userRepository = $userRepository;
        $this->commentsRepo = $commentsRepo;
    }

    public function run()
    {
        $action = array_key_exists('action', $_GET) ? $_GET['action'] : 'index';

        if (array_key_exists('email', $_SESSION)) {
            $this->params['username'] = $_SESSION['email'];
        }

        if (array_key_exists('loginfo', $_SESSION)) {
            $this->params['loginfo'] = $_SESSION['loginfo'];
        }

        if (array_key_exists('loginfoerror', $_SESSION)) {
            $this->params['loginfoerror'] = $_SESSION['loginfoerror'];
        }

        switch ($action) {
            case 'logout':
                session_destroy();
                $_SESSION['loginfo'] = '';
                header('Location: index.php?action=index');
                break;
            case 'login':
                $this->login();
                break;
            case 'attraction-list':
                $this->params['atractions'] = $this->attractionList();
                $templateName = 'chossenAtraction.html';
                break;

            case 'top5':
                $this->params['atractions'] = $this->repo->getTopAtraction();
                $templateName = 'index.html';
                break;
            case 'forKids':
                $this->params['atractions'] = $this->repo->getAtractionForKids();
                $templateName = 'index.html';
                break;

            case 'index':
                $this->params['atractions'] = $this->repo->getAtractions();
                $templateName = 'index.html';
                break;
            case 'contact':
                $templateName = 'contact.html';
                break;
            case 'registration':
                $templateName = 'registration.html';
                $this->register();
                break;
            case 'allAboutAtraction':
                $atractionId = array_key_exists('atractionId', $_GET) ? $_GET['atractionId'] : $_SESSION['atractionId'];
                $onlyUserName = array_key_exists('email', $_SESSION) ? explode('@',$_SESSION["email"]) : null;
                $this->params['actualUser']=$onlyUserName[0];
                $this->params['atractions']=$this->repo->getAtraction($atractionId);
                $this->params['comments'] = $this->commentsRepo->getComments($atractionId);
                $this->params['comments'] = $this->cutMail($this->params['comments']);
                $this->repo->addOneVisit($atractionId);

                $templateName = 'allAboutAtraction.html';
                break;
            case 'addComment':
                $this->newComment($_POST['textNewComment'],$_POST['atractionId']);
                $_SESSION['atractionId'] = $_POST['atractionId'];
                header('Location: index.php?action=allAboutAtraction');
                break;
            case 'removeComment':
                $this->deleteComment($_POST['id_comment']);
                $_SESSION['atractionId'] = $_POST['atractionId'];

                header('Location: index.php?action=allAboutAtraction');
                break;
            case 'inProgress':
                $templateName = 'inProgres.html';
                break;
            default:
                $templateName = 'error404.html';
                break;
        }

        $this->template->renderTemplate($templateName, $this->params);
    }

    private function  attractionList()
    {
        $city = $_POST['place'];
        $category = $_POST['category'];
        $find = $_POST['find'];
        return ($this->repo->getChossenAtraction($city, $category,$find));
    }

    private function login()
    {
        $user = new User(htmlspecialchars($_POST['email']), htmlspecialchars($_POST['password']), $this->userRepository);

        if ($user->validateUser())
        {
            $_SESSION['userId'] = $this->userRepository->getUserID($_POST['email']);
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['loginfo'] = 'Witaj ' . $_POST['email'];
            $_SESSION['loginfoerror'] = null;
        } else {
            $_SESSION['loginfoerror'] = 'Nieprawidlowy login lub hasło';

        }
        header('Location: index.php?action=index');

    }
    private function register()
    {
        if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)
            && array_key_exists('password2', $_POST)
        ) {

            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
                $this->params['loginfo'] = 'Podana wartość nie jest mailem.';
                return false;
            } else if (strlen($_POST['password']) < 8) {
                $this->params['loginfo'] = 'Hasło musi mieć powyżej 8 znaków.';
                return false;
            } else if ($_POST['password'] != $_POST['password2']) {
                $this->params['loginfo'] = 'Wpisane hasła nie są takie same';
                return false;
            }

            $user = new User(htmlspecialchars($_POST['email']), htmlspecialchars($_POST['password']), $this->userRepository);

            if ($user->loginAlreadyExists()) {
                $this->params['loginfo'] = 'Podany e-mail już istnieje';

            } else {
                $user->newUser();

                $_SESSION['email'] = $_POST['email'];
                $_SESSION['loginfo'] = 'Witaj ' . $_SESSION['email'];
                $_SESSION['userId'] = $this->userRepository->getUserID($_POST['email']);

                header('Location: index.php?action=index');
            }
    }

    }

    private function newComment($commentText,$atractionId)  {

        return $this->commentsRepo->addComment($commentText,$atractionId);
    }

    private function cutMail($comments){

        foreach ($comments as &$oneCom) {
            $onlyName = explode('@',$oneCom["user_email"]);
            $oneCom['user_email'] = $onlyName[0];
        }
        return $comments;
    }

    private function deleteComment($id_comment){

        return $this->commentsRepo->removeComment($id_comment);
    }





}


