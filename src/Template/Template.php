<?php

namespace Sda\Atrakcjetrojmiasta\Template;

use Twig_Environment;

class Template
{

    private $twig;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function renderTemplate($templateName, array $parameters)
    {
        echo $this->twig->render($templateName, $parameters);
    }


}
