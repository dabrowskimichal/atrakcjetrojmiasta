<?php


namespace Sda\Atrakcjetrojmiasta\User;

use Doctrine\DBAL\Connection;

class User  {

    private $username;

    private $password;

    /**
     * @var UserRepository
     */
    private $repo;


    public function __construct($username, $password, UserRepository $repo) {
        $this->username = $username;
        $this->password = $password;
        $this->repo = $repo;
    }

    public function validateUser()
    {
        $user=$this->repo->getUserByName($this->username);
        if(false !== $user && hash_equals($user["user_password"], crypt($this->password, $user["user_password"])))
        {
            return true;
        }
        return false;
    }

    public function loginAlreadyExists() {
        if ($user=$this->repo->getUserByName($this->username)){
            return true;
         }
    }

    public function newUser() {
        if ($user=$this->repo->addUser($this->username,$this->password)){

            return true;
        }
        return false;
    }

}


