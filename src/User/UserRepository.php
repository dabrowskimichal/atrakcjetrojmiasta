<?php
/**
 * Created by PhpStorm.
 * User: RENT
 * Date: 2017-04-29
 * Time: 09:30
 */

namespace Sda\Atrakcjetrojmiasta\User;

use Doctrine\DBAL\Connection;


class UserRepository
{
    protected $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    public function getUserByName($username){
        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `user_email` = :username');
        $sth->bindValue('username', $username, \PDO::PARAM_INT);
        $sth->execute();
        $user = $sth->fetch();
        return $user;
    }


    public function addUser($email,$pass) {
        $pass = crypt($pass, $pass);
        $addNewUser = $this->dbh->insert('users',array('user_email'=>$email));
        $addNewUser = $this->dbh->update('users',array('user_password'=>$pass),array('user_email'=>$email));

        return true;
    }


    public function getUserID($user)
    {
        $sth = $this->dbh->prepare('SELECT user_id FROM `users` WHERE `user_email` = :user');
        $sth->bindValue('user', $user, \PDO::PARAM_INT);
        $sth->execute();
        $record = $sth->fetch();

        return $record['user_id'];
    }


}

