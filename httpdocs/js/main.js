

$(document).ready(function(){


    $('#carousel-example-generic .carousel-inner div.item:first-child').addClass('active');

    $('.carousel').carousel({
        pause: null
    });

    $(document).on('change', '#CityChoice, #Category, #find', function(e){

        var city = $('#CityChoice').val();
        var category = $('#Category').val();
        var find = $('#find').val();

        $.ajax({
            url: 'index.php?action=attraction-list',
            type: 'POST',
            data: {
                "place": city,
                "category": category,
                "find":find
            },
            success: function(ret) {
                $('#attractions').html(ret);
                $('#CityChoice').val(city);
                $('#Category').val(category);
            },
            error: function(err) {
                // console.log(err);
                alert('aaa');
            }
        });
        e.preventDefault();

    });

});
