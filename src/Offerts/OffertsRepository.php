<?php

namespace Sda\Atrakcjetrojmiasta\Offerts;

use Doctrine\DBAL\Connection;


class OffertsRepository
{

    protected $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }


    public function getAtraction($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `atraction` WHERE `id_atraction` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $record = $sth->fetch();
        return $record;
      }


    public function getAtractions()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `atraction`');
        $sth->execute();
        $records = $sth->fetchAll();
               
        return $records;
          }

    public function getChossenAtraction($city, $category,$find)
    {
        $query = 'SELECT * FROM `atraction` WHERE';


        if ($find === 'Szukaj') {
            $find = '';
        };
        $find ="%" . $find . "%";


        if ($city !== '0'){
            $query .= ' `city` = :city';
            };

        if (($city !== '0') && ($category !== '0')){
            $query .= ' and ';
             };

        if ($category !== '0'){
            $query .= ' `category` = :category';
        };

        if (($city !== '0') && ($category === '0') && ($find !== '') ){
            $query .= ' and ';
        };

        if (($find !== '') && ($category !== '0')){
            $query .= ' and ';
        };

        if ($find !== ''){
            $query .= ' `name_atraction` LIKE :find';
        };

        $sth = $this->dbh->prepare($query);
        if ($city !== '0') {
            $sth->bindValue('city', $city, \PDO::PARAM_STR);
        }
        if ($category !== '0') {
            $sth->bindValue('category', $category, \PDO::PARAM_STR);
        }
        if ($find !== '') {
            $sth->bindValue('find', $find, \PDO::PARAM_STR);
        }

        $sth->execute();
        $records = $sth->fetchAll();

        return $records;
    }

    public function getTopAtraction()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `atraction` ORDER BY `nr_of_visit` DESC');
        $sth->execute();
        $records = $sth->fetchAll();
        $topAtractions = array();
        for ($x = 0; $x<=4 ; $x++){
            $topAtractions[$x]= $records[$x];
        }

        return $topAtractions;


    }

    public function getAtractionForKids()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `atraction` WHERE `for_kids`=:yes ORDER BY `nr_of_visit` DESC');
        $sth->bindValue('yes', 1, \PDO::PARAM_INT);
        $sth->execute();
        $records = $sth->fetchAll();
        return $records;
    }

    public function addOneVisit($id_atraction){
        $sth = $this->dbh->prepare('SELECT `nr_of_visit`FROM `atraction` WHERE `id_atraction` = :id');
        $sth->bindValue('id', $id_atraction, \PDO::PARAM_INT);
        $sth->execute();
        $visitsNumber = $sth->fetch();
        $visitsNumber =$visitsNumber["nr_of_visit"] + 1;
        $addOneView = $this->dbh->update('atraction',array('nr_of_visit'=>$visitsNumber),array('id_atraction'=>$id_atraction));
        return true;

    }




}
