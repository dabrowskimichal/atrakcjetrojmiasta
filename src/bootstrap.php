<?php


session_start();

require_once __DIR__ . '/../vendor/autoload.php';

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Atrakcjetrojmiasta\Config\Config;
use Sda\Atrakcjetrojmiasta\Controller\Controller;
use Sda\Atrakcjetrojmiasta\Config\Routing;
use Sda\Atrakcjetrojmiasta\Request\Request;
use Sda\Atrakcjetrojmiasta\Template\Template;
use Sda\Atrakcjetrojmiasta\Offerts\OffertsRepository;
use Sda\Atrakcjetrojmiasta\User\UserRepository;
use Sda\Atrakcjetrojmiasta\Comment\CommentRepository;

$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    [
        'cache' => false
    ]
);

$config = new Configuration();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);

$request = new Request();
$template = new Template($twig);
$repo = new OffertsRepository($dbh);
$userRepo = new UserRepository($dbh);
$comments = new CommentRepository($dbh);

$app = new Controller($request,$template, $repo,$userRepo,$comments);

$app->run();