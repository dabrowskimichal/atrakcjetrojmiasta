<?php

namespace Sda\Atrakcjetrojmiasta\Comment;

use Doctrine\DBAL\Connection;
use Doctrine\Tests\Common\Annotations\True;


class CommentRepository
{

    protected $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    public function addComment($commentText, $atractionId)
    {
        $newComment = new Comment($_SESSION['userId'], $atractionId, $commentText);

        if ($newComment) {

            return $addedcomment = $this->dbh->insert('comments', [
                'id_user' => $_SESSION['userId'],
                'id_atraction' => $atractionId,
                'comment' => $commentText

            ]);

        }

        return false;
    }

    public function getComments($atractionId)
    {
        $sth = $this->dbh->prepare(
            'SELECT
              c.id_comment,
              c.comment,
              c.date,
              u.user_email
            FROM 
              `comments` AS c 
            JOIN `atraction` AS a ON a.id_atraction = c.id_atraction
            JOIN `users` AS u ON c.id_user = u.user_id
            WHERE
              c.`id_atraction` = :id AND  c.`admin_accept` = :accepted ORDER BY `date` DESC'
        );

        $sth->bindValue('id', $atractionId, \PDO::PARAM_INT);
        $sth->bindValue('accepted', 1, \PDO::PARAM_INT);

        $sth->execute();
        $record = $sth->fetchAll();
        return $record;
    }

    public function removeComment($idComment)
        {
        return $this->dbh->delete('comments', array(
        'id_comment' => $idComment
        ));
        }

}

