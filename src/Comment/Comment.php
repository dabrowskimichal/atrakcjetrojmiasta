<?php


namespace Sda\Atrakcjetrojmiasta\Comment;

class Comment
{
    private $id_comment;

    private $id_user;

    private $id_atraction;

    private $commentText;

    public function __construct($id_user, $id_atraction, $commentText, $id_comment = null)
    {
        $this->id_comment = $id_comment;
        $this->id_user = $id_user;
        $this->id_atraction = $id_atraction;
        $this->commentText = $commentText;
    }

    /**
     * @return mixed
     */
    public function getIdComment()
    {
        return $this->id_comment;
    }
}


